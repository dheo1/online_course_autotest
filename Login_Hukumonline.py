import time

from selenium import webdriver
from selenium.webdriver.common.by import By


driver = webdriver.Chrome()

driver.get("https://id.hukumonline.com/user/login/")
time.sleep(5)
# driver.title # => "Google"

# driver.implicitly_wait(0.5)

username_field = driver.find_element(By.ID, "username").send_keys("iqbal-standard")
time.sleep(2)
password_field = driver.find_element(By.ID, "password").send_keys("iqbal-standard")
time.sleep(2)
login_button = driver.find_element(By.ID, "submit").click()
time.sleep(2)

driver.close()

# driver.find_element(By.NAME, "q").get_attribute("value") # => "Selenium"

# driver.quit()